import time
import chromadb
import cv2
from utils import face_rects
from chromadb.config import Settings

from utils import face_encodings

client = chromadb.HttpClient(host='localhost', port=8000, settings=Settings(allow_reset=True))
collection = client.get_or_create_collection(name="name_collection")

image = cv2.imread("examples/GD1.jpg")
encodings = face_encodings(image)
names = []

for encoding in encodings:
    res = list(collection.query(
        query_embeddings=encoding,
        include=["documents", "distances"],
        n_results=1
    ).values())

    threshold = float(res[1][0][0]) <= 0.2

    if threshold:
        name = res[4][0][0]
    else:
        name = "Unknown"

    names.append(name)

for rect, name in zip(face_rects(image), names):
    # get the bounding box for each face using the `rect` variable
    x1, y1, x2, y2 = rect.left(), rect.top(), rect.right(), rect.bottom()
    # draw the bounding box of the face along with the name of the person
    cv2.rectangle(image, (x1, y1), (x2, y2), (0, 255, 0), 2)
    cv2.putText(image, name, (x1, y1 - 10),
            cv2.FONT_HERSHEY_SIMPLEX, 0.75, (0, 255, 0), 2)

# show the output image
cv2.imshow("image", image)
cv2.waitKey(0)




