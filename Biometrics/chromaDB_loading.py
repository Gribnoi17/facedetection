import chromadb
from chromadb.config import Settings
import os
from face_encoding import get_embedding_dict

client = chromadb.HttpClient(host='localhost', port=8000, settings=Settings(allow_reset=True))
# client.reset()
collection = client.get_or_create_collection(name="name_collection")

root_dir = "dataset"
class_names = os.listdir(root_dir)
embeddings_dict = get_embedding_dict(root_dir, class_names)

for name, embeddings in embeddings_dict.items():
    id = 1
    name_id = name + str(id)
    for embedding in embeddings:
        collection.add(
            documents=name,
            embeddings=embedding,
            ids=str(name_id)
        )
        id += 1
        name_id = name + str(id)


