import cv2
import time
import chromadb

from chromadb.config import Settings
from utils import face_rects
from utils import face_encodings

client = chromadb.HttpClient(host='localhost', port=8000, settings=Settings(allow_reset=True))
collection = client.get_or_create_collection(name="name_collection")

# Initialize the webcam
cap = cv2.VideoCapture(0)

last_comparison_time = -5  # Инициализируем время последнего сравнения

while True:
    # Capture frame-by-frame
    ret, frame = cap.read()
    current_time = time.time()  # Получаем текущее время

    # Проверяем, прошло ли 5 секунд с момента последнего сравнения
    if current_time - last_comparison_time >= 5:
        imgS = cv2.resize(frame, (0,0), None, 0.25, 0.25)
        # get the 128-d face embeddings for each face in the input frame
        encodings = face_encodings(imgS)

        # this list will contain the names of each face detected in the frame
        names = []

        # loop over the encodings
        for encoding in encodings:
            res = list(collection.query(
                query_embeddings=encodings[0],
                include=["documents", "distances"],
                n_results=1
            ).values())

            threshold = float(res[1][0][0]) <= 0.2

            if threshold:
                name = res[4][0][0]
            else:
                name = "Unknown"

            names.append(name)

        last_comparison_time = current_time  # Обновляем время последнего сравнения

    # loop over the `rectangles` of the faces in the
    # input frame using the `face_rects` function
    for rect, name in zip(face_rects(frame), names):
        # get the bounding box for each face using the `rect` variable
        x1, y1, x2, y2 = rect.left(), rect.top(), rect.right(), rect.bottom()
        # draw the bounding box of the face along with the name of the person
        cv2.rectangle(frame, (x1, y1), (x2, y2), (0, 255, 0), 2)
        cv2.putText(frame, name, (x1, y1 - 10),
                    cv2.FONT_HERSHEY_SIMPLEX, 0.75, (0, 255, 0), 2)

    # Display the resulting frame
    cv2.imshow('Frame', frame)

    # Подождать 1 миллисекунду перед продолжением выполнения цикла
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break


# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()
